package pl.niewiel.pracadyplomowa.httpclient;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.orm.SugarRecord;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import pl.niewiel.pracadyplomowa.database.model.Token;

public class ApiClient {


    private Context context;

    public ApiClient(Context context) {
        this.context = context;
    }


    // Instantiate the RequestQueue.
    @SuppressLint("TrulyRandom")
    private static void handleSSLHandshake() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @SuppressLint("TrustAllX509TrustManager")
                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @SuppressLint("TrustAllX509TrustManager")
                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @SuppressLint("BadHostnameVerifier")
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception ignored) {
        }
    }


    public void getClientToken() {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        String url = "https://devdyplom.nuc-mleczko-pawel.pl/oauth/getToken";
        handleSSLHandshake();
        HashMap<String, String> params = new HashMap<>();
        params.put("grant_type", "client_credentials");

        JsonObjectRequest req = new JsonObjectRequest(url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (!response.toString().equals("")) {
                            Token token = null;
                            // handle response
                            try {
                                token = new Token(
                                        response.getString("access_token"),
                                        response.getString("expires_in"),
                                        response.getString("token_type"),
                                        null);
                                Toast.makeText(context, token.toString(), Toast.LENGTH_LONG).show();
                                Log.i("TOKEN", token.toString());

                                SugarRecord.save(token);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            assert token != null;
                            Log.i("token", token.toString());
                        } else {
                            Log.e("ERROR", "response no exists");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // handle error
                Log.e("ERROR", error.toString());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Basic MDFhYTEyMzA0YTc0NWQ3ZmIyY2ZiMDUxYjMxZGUyMmY6JDJ5JDExJHp1R0lPOG5Ga3lJcU1QMVBCY0VaVWVHUEVlWll0elJOaTFSTjNKUlZ5amFXYTdGRllUZEJL");
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };

        requestQueue.add(req);
    }

    public void getUserToken(String user, String password) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        String url = "https://devdyplom.nuc-mleczko-pawel.pl/oauth/getToken";
        handleSSLHandshake();
        HashMap<String, String> params = new HashMap<>();
        params.put("grant_type", "password&username=" + user + "&password=" + password);

        JsonObjectRequest req = new JsonObjectRequest(url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (!response.toString().equals("")) {
                            Token token = null;
                            // handle response
                            try {
                                token = new Token(
                                        response.getString("access_token"),
                                        response.getString("expires_in"),
                                        response.getString("token_type"),
                                        response.getString("refresh_token"));
                                SugarRecord.save(token);
                                Log.i("TOKEN", token.toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            assert token != null;
                            Log.i("token", token.toString());
                        } else {
                            Log.e("ERROR", "response no exists");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // handle error
                Log.e("ERROR", error.toString());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Basic MDFhYTEyMzA0YTc0NWQ3ZmIyY2ZiMDUxYjMxZGUyMmY6JDJ5JDExJDAuSWwwUE1NU1VtSENEZjVTRU1WOC5KYWpNZ3lFaHlyNXdtc2pURkdjemhOazJva2FTVkl5");
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };

        requestQueue.add(req);
    }


}
