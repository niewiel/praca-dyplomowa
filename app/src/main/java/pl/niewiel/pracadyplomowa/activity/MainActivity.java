package pl.niewiel.pracadyplomowa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.orm.SugarRecord;

import java.util.List;

import pl.niewiel.pracadyplomowa.R;
import pl.niewiel.pracadyplomowa.database.model.Token;
import pl.niewiel.pracadyplomowa.httpclient.ApiClient;

public class MainActivity extends AppCompatActivity {
    ApiClient client;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button cameraButton = findViewById(R.id.buttonCamera);
        Button getTokenButton = findViewById(R.id.getTokenButton);
//        test();
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });
        getTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getToken();
            }
        });
    }

    public void openCamera() {
        Intent intent = new Intent(this, Camera.class);
        startActivity(intent);
    }

    private void test() {

        List<Token> tokens = SugarRecord.findWithQuery(Token.class, "select * from token");
        Toast.makeText(getApplicationContext(), tokens.toString(), Toast.LENGTH_LONG).show();
        Log.e("Database:", "token=" + tokens.toString());

    }

    private void getToken() {
        client = new ApiClient(this);
        client.getUserToken("user123", "user123");
    }
}
