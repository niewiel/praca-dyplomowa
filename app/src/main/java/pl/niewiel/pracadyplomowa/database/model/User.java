package pl.niewiel.pracadyplomowa.database.model;

import com.orm.dsl.Column;
import com.orm.dsl.Table;

import java.sql.Date;


@Table
public class User {

    @Column(name = "date_add")
    private Date dateAdd;
    @Column(name = "date_edit")
    private Date dateEdit;
    @Column(name = "bs_id")
    private int bsId;
    private String name;


}
